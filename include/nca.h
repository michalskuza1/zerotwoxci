#pragma once

#include "pfs0.h"
#include "baecb.h"

#include <stdint.h>
#include <stdbool.h>
typedef struct {
  char magic[4];
  uint32_t numfiles;
  uint32_t string_table_size;
  uint32_t reserved;
  uint64_t offsetoffile;
  uint64_t sizeofile;
  uint32_t sizeofstringtable;
  uint32_t reservedfiletable;
} pfso_header_t;
typedef struct __attribute__((packed)) {
    uint32_t media_offset; // multiply by 0x200 for actual offset
    uint32_t media_end; // ditto
    uint32_t unknown1;
    uint32_t unknown2;
} nca_section_entry_t;

typedef struct __attribute__((packed)) {
    char master_hash[32];
    uint32_t blocksize;
    uint32_t unknown_0; // Must be 0x2
    uint64_t hashtable_offset; // Relative to section start (usually 0)
    uint64_t hashtable_size;
    uint64_t header_offset; // Relative to section start (usually just after end of hashtable)
    uint64_t fs_size;
} nca_pfs0_superblock_t;

typedef struct __attribute__((packed)) {
    char magic[8]; // IVFC + 0x20000
    char master_hash_size[4]; // maybe?
    char unknown_1[4];
    struct
    {
        uint64_t offset;
        uint64_t size;
        uint32_t blocksize; // log2
        uint32_t reserved;
    } levels[6];
    char unknown_2[32];
    char hash[32];
} nca_romfs_superblock_t;

enum {
    NCA_PARTITION_TYPE_ROMFS = 0,
    NCA_PARTITION_TYPE_PFS0 = 1,
};

enum {
    NCA_FSTYPE_PFS0 = 2,
    NCA_FSTYPE_ROMFS = 3,
};

enum {
    NCA_SECTION_CRYPTO_TYPE_PLAINTEXT = 1,
    NCA_SECTION_CRYPTO_TYPE_XTS = 2,
    NCA_SECTION_CRYPTO_TYPE_CTR = 3,
};

typedef struct __attribute__((packed)) {
    char unknown_0[2];
    uint8_t partition_type;
    uint8_t fstype;
    uint8_t cryptotype;
    char unknown_1[3];
    union {
        char superblock_padding[312];
        nca_pfs0_superblock_t pfs0;
        nca_romfs_superblock_t romfs;
    };
    char section_ctr[8];
    char unknown_2[184]; // Apparently BKTR headers live here... cross that bridge later
}  nca_section_header_t;

enum {
    NCA_LOCATION_CONSOLE = 0,
    NCA_LOCATION_GAMECARD = 1,
};

enum {
    NCA_CONTENT_TYPE_PROGRAM = 0,
    NCA_CONTENT_TYPE_META = 1,
    NCA_CONTENT_TYPE_CONTROL = 2,
    NCA_CONTENT_TYPE_MANUAL = 3,
    NCA_CONTENT_TYPE_DATA = 4,
};

enum {
    NCA_CRYPTO_TYPE1_PRE_3 = 0,
    NCA_CRYPTO_TYPE1_300 = 2,
};

enum {
    NCA_CRYPTO_TYPE2_301 = 3,
    NCA_CRYPTO_TYPE2_400 = 4,
    NCA_CRYPTO_TYPE2_500 = 5,
};

typedef struct __attribute__((packed)) {
    char static_sig[256];
    char npdm_sig[256];
    char magic[4];
    uint8_t location;
    uint8_t content_type;
    uint8_t crypto_type1;
    uint8_t key_index;
    uint64_t filesize;
    uint64_t titleid;
    char unknown_0[4]; // Padding? Not mentioned on switchbrew wiki
    uint32_t sdk_version;
    uint8_t crypto_type2;
    char unknown_1[15]; // More padding?
    char rights_id[16];
    nca_section_entry_t section_table[4];
    char hashes[4][32];
    char keys[4][16];
    char padding[192];
    nca_section_header_t section_headers[4];
} nca_header_t;

// Translate a CNMT content type to an NCA content type
uint8_t nca_content_type_from_cnmt(uint8_t flag);

// Fill in a header with defaults/blank
void nca_init_header(nca_header_t* header, nca_header_t* old_hd);

// Decrypt header
void nca_decrypt_header(nca_header_t* out, char* in);

// Encrypt header
void nca_encrypt_header(char* out, nca_header_t* in);

// Get offset of section in file
uint64_t nca_offsetof_section(nca_header_t* header, unsigned int section);

// Decrypt an NCA section in memory
void nca_decrypt_section(nca_header_t* header, unsigned int section, char* out, char* in, uint64_t size);

// Encrypt an NCA section in memory
void nca_encrypt_section(nca_header_t* header, unsigned int section, char* out, char* in, uint64_t size);

// Extract section from NCA and write out in plaintext
// Callback "in" must begin reading at the start of the encrypted section
// Callback "out" will likewise begin writing at the start of the decrypted section
void nca_extract_section(nca_header_t* header, unsigned int section,
        baecb_write out, void* outcbdata,
        baecb_read in, void* incbdata);

// Replace section in NCA encrypting according to header
// Callback "in" must begin reading at the start of the decrypted section
// Callback "out" will likewise begin writing at the start of the encrypted section
void nca_replace_section(nca_header_t* header, unsigned int section,
        baecb_write out, void* outcbdata,
        baecb_read in, void* incbdata);

// Add a PFS0 section to the NCA
void nca_inject_pfs0(nca_header_t* header, unsigned int section,
        baecb_write out, void* outcbdata,
        baecb_read in, void* incbdata, uint64_t fs_size);

// Calculate the size of a section from its table entry
unsigned long int nca_sizeof_section(nca_header_t* header, unsigned int section);

// Determine whether the NCA has a rightsID (i.e. titlekey crypto)
bool nca_has_rightsid(nca_header_t* header);
